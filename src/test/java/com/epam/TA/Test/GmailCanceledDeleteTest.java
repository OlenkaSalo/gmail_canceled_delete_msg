package com.epam.TA.Test;


import com.epam.TA.Pages.GmailPageObject;
import org.openqa.selenium.By;
import org.openqa.selenium.support.ui.ExpectedConditions;
import org.testng.Assert;
import org.testng.annotations.Test;
public class GmailCanceledDeleteTest extends BaseTest {

    @Test
    public void simplePageObjectTest() {
        GmailPageObject gmailPageObject = new GmailPageObject(driver);

        gmailPageObject.typeLoginAndSubmit("testng.userdrive@gmail.com");


        wait.until(ExpectedConditions.visibilityOfElementLocated(By.xpath("//input[@type='password']")));
        gmailPageObject.typePswAndSubmit("1111test");

        gmailPageObject.selectThreeMsg();

        gmailPageObject.deleteMsg();

        Assert.assertTrue(gmailPageObject.verifyCanceledAction());


    }


}

