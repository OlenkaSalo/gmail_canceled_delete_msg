package com.epam.TA.Test;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.chrome.ChromeDriver;
import org.openqa.selenium.support.ui.WebDriverWait;
import org.testng.annotations.AfterClass;
import org.testng.annotations.BeforeClass;
import org.testng.annotations.BeforeSuite;

import java.util.concurrent.TimeUnit;

public class BaseTest {

    public static final Logger LOG = LogManager.getLogger(BaseTest.class);
    public static WebDriver driver;
    public WebDriverWait wait;

    @BeforeSuite(alwaysRun = true)
    public void configSuite() {

        LOG.info("===================================");
        LOG.info("   TESTS  VALID - STARTED");
        LOG.info("===================================");
    }


    @BeforeClass
    public void setSystem(){
        System.setProperty("webdriver.chrome.driver", "src/main/resources/chromedriver.exe");
        driver = new ChromeDriver()  {
            {
                manage().timeouts().implicitlyWait(10, TimeUnit.SECONDS);
            }
        };
        wait = new WebDriverWait(driver,10);
        driver.get("https://www.gmail.com");


    }

    @AfterClass
    public void closeAll()
    {
        driver.quit();
        LOG.info("Test end successful!!");
    }

}
