package com.epam.TA.Pages;

import com.epam.TA.Test.BaseTest;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.PageFactory;
import java.util.List;



public class GmailPageObject {

    public static final Logger LOG = LogManager.getLogger(GmailPageObject.class);

    @FindBy(id = "identifierId")
    private WebElement loginInput;

    @FindBy(xpath = "//div[@id='identifierNext']")
    private WebElement nextButton;

    @FindBy(xpath = "//div//input[@type='password']")
    private WebElement pswInput;

    @FindBy(xpath = "//div[@id='passwordNext']")
    private WebElement nextButtonPsw;

    @FindBy(xpath = "//td[@class = 'oZ-x3 xY']//div[@role='checkbox']")
    private List<WebElement> selectMsg;

    @FindBy(xpath = "//*[@id=':4']/div/div[1]/div[1]/div/div/div[2]/div[3]")
    private WebElement deleteButton;

    @FindBy(xpath = "//div[@class='vh']//span[@id='link_undo']")
    private WebElement canceleDel;

    @FindBy(xpath = "//div[@class='vh']//span[@class='aT']")
    private WebElement canceledAlert;


    public GmailPageObject(WebDriver driver){
        PageFactory.initElements(driver, this);
    }

    public void typeLoginAndSubmit(String login){
        loginInput.sendKeys(login);
        nextButton.click();
        LOG.info("Username link account found");
    }

    public void typePswAndSubmit(String login){

        pswInput.sendKeys(login);
        nextButtonPsw.click();
        LOG.info("Password element found");
    }

    public void selectThreeMsg()
    {
        for(int i=0; i<3; i++)
        {
            selectMsg.get(i).click();
        }
        LOG.info("Three message selected");
    }

    public void deleteMsg()
    {
        deleteButton.click();

        LOG.info("Delete element found");

        canceleDel.click();

        LOG.info("Canceled element found");
    }

    public boolean verifyCanceledAction()
    {

        if(canceledAlert.isDisplayed())
        {
            return true;
        }
        else
            LOG.info("Delete massege canceled");
        return false;
        
    }
}
